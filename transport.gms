

* First we create the sets. I arbitrarily chose to use capitalize
* the keyword SETS. However, it has the same effect as Sets, sets, or sEtS
* as GAMS does not distinguish between lower and upper-case.

SETS
i set of plants /p1*p2/
j set of markets /m1*m3/;


* Then we introduce the parameters.

PARAMETERS
a(i) capacity of plant i in units
         /p1 350
          p2 600/
	  
b(j) demand of market j in units
         /m1 325
          m2 300
          m3 275/;
* What happens if I do not assign a value to m3?
* What happens if I add "m4 450"?

TABLE d(i,j) distance from i to j in kMiles
                 m1      m2      m3
         p1      2.5     1.7     1.8
         p2      2.5     1.8     1.4;

SCALAR f freight in dollars per kMiles /90/;


PARAMETER c(i,j) transport cost in kDollars per unit;
	  c(i,j) = f * d(i,j) / 1000;
	  

* We define the decision variables

VARIABLES
z        value of the objective function in kDollars
x(i,j)   number of units shipped from i to j;


* Then we state their type
POSITIVE VARIABLE x(i,j);


* First we declare the equations
EQUATIONS
cost             the total transportation cost
supply(i)          the amount shipped from plant i
demand(j)       the amount received by market j;

* Then we define them
cost ..	           z =e= sum((i,j), c(i,j)*x(i,j));
supply(i)..        sum(j,x(i,j)) =l= a(i);
demand(j)..        sum(i,x(i,j)) =g= b(j);


* Time Limit
OPTION reslim = 1e10;

* How many variables and respective coefficients
* of each kind should the model show in the log file
* See columns listing in the lst file
OPTION limcol = 2;

* How many constraints of each kind should the model show in the log file
* See equations listing in the lst file
OPTION limrow = 2;

* We create a model named "transport" which includes all the elements we defined
MODEL transport /all/;

* We solve the model as a linear program minimizing z
SOLVE transport USING lp MINIMIZING z;

* We display some solution information

DISPLAY "SOLVER INFORMATION",

* The status of the solution algorithm,
* i.e., whether the algorithm found that the
* problem is optimal, local optimal, feasible, infeasible, etc.
* See page 90 of the user manual
transport.modelstat,

* Whether the algorithm terminated normally or with errors
* or was interrupted by e.g., expired time limit
* See page 90
transport.solvestat,

* The time spent solving the problem
transport.resusd

* The solution
x.l

* The reduced cost
x.m
;